//se definen bibliotecas
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
//se define estructura
typedef struct{
	char facultad[254],carrer[256],codigo[8],nem[4],rank[4],leng[4],mat[4],hist[4],cs[4],pond[8],psu[8],max[8],min[8],cpsu[4],bea[2];
}carrera;
bool comparaStr (char entrada[],char modelo[])//se define un tipo tipo de dato booleano
{
int ind = 0;//se define variable de tipo entero
while (entrada[ind]!='\0' && modelo[ind]!='\0' && entrada[ind] == modelo[ind]) ind++;//bucle para comparar strings
if (entrada[ind]!='\0' || modelo[ind]!='\0')//mientras las entradas o el modelo sean igual a \0
   return false;//su retorno sera falso

return true;//en caso de que la condicion no se cumpla se seguira con el programa y se retornara verdadero
}
int menu(){
	int opcion;
	printf("\nmenu:\n1.listar facultades\n2.listar carreras\n3.buscar una facultad\n4.buscar una carrera\n");//se imprime el menu
	scanf("%d",&opcion);//se pide al usuario que ingrese una opcion
	return opcion;
}

int main(){//comienza el cuerpo del programa
	int opcion=0,control=0;//se define variables de tipo entero
	while(opcion!=4){//mientras la opcion sea igual a 4 entonces ocurrira lo siguiente
	opcion=menu();
	int opcion;//se define variable tipo entero
	carrera todo[53];//se define arreglo de 53 espacios
	if (opcion==1){//si la opcion del usuario es igual a 1 entonces ocurrira lo siguiente
		//se imprimiran las facultades solicitadas
		printf("facultad de arquitectura\nfacultad de ciencias\nfacultad de ciencias del mar y de recursos natural\nfacultad de ciencias economicas y administrativas\nfacultad de derecho y ciencias sociales\nfacultad de farmacia\n facultad de humanidades\nfacultad de ingenieria\nfacultad de medicina\nfacultad de odontologia");
		
	}
	if (opcion==2){//si la opcion ingresada por el usuario es igual a 2 ocurrira lo siguiente
		int i=0;//se define variable de tipo entero
		FILE *file;//se crea puntero para el archivo
		file=fopen("facultad_de_arquitectura.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_ciencias.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_ciencias_del_mar_y_de_recursos_natural.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s  %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_ciencias_economicas_y_administrativas.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_derecho_y_ciencias_sociales.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_farmacia.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_humanidades.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;
		}
		file=fopen("facultad_de_ingenieria.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_medicina.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		file=fopen("facultad_de_odontologia.txt","r");//se abre el archivo en modo lectura
		while (feof(file)==0){//mientras se lea hasta el final el archivo
			//se lee hasta el final y se le asigna a una variable
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",todo[i].facultad,todo[i].carrer,todo[i].codigo,todo[i].nem,todo[i].rank,todo[i].leng,todo[i].mat,todo[i].hist,todo[i].cs,todo[i].pond,todo[i].psu,todo[i].max,todo[i].min,todo[i].cpsu,todo[i].bea);
			printf("%s\n",todo[i].carrer);//se imprime resultado
			i++;//se aumenta en 1 la variable i
		}
		control=1;//control va a ser igual a 1
		

		
	}
	if (opcion==3){//si la opcion del usuario es igual a 3 entonces ocurrira lo siguiente
		if (control==1){//condicional para ver si el control es igual a 1
		char nombre[254];//se define arreglo de tipo char con 254 espacios
		int i;//se define variable de tipo entero
		printf("ingrese la facultad que desea buscar (en vez de un espacio use un guion bajo)\n");
		scanf("%s",&nombre);//se pide al usuario que ingrese la facultad que desea buscar
 		for (i=0;i<53;i++){//se usa la variable i para recorrer la peticion del usuario
 			if (( comparaStr (nombre,todo[i].facultad))){//si esta coincide con los requerimientos del usuario
 				printf("%s\n",todo[i].carrer);//se imprime la facultad solicitada
 			}
 		}	
		}
		if (control==0){//condicional para ver si el control es igual a 0
			printf("cargue los archivos primero en la opcion 2");//imprime mensaje para cargar archivo
		}
		else{
			printf("no ingreso correctamente el nombre");//de no coincidir la facultad solicitada con lo que escribio el usuario entonce
		}
}
	if (opcion==4){//si la opcion ingresada por el usuario es igual a 4 entonces ocurrira lo siguiente
		if (control==1){//si control es igual a 1 entonces ocurrira lo siguiente
		int i;//se define variable de tipo entero
		char nombre[254];//arreglo de tipo char con 254 espacios
		printf("ingrese la carrera que desea buscar\n");//imprime mensaje para el usuario
		scanf("%s",&nombre);//se pide al usuario que ingrese la carrera
		for (i=0;i<53;i++){//se usa la variable para recorr el requerimiento del usuario
 			if (( comparaStr (nombre,todo[i].carrer))){//si coinciden entonces  ocurre lo siguiente
 				//se imprime la informacion solicitada
 				printf("\ncarrera encontrada\n");
 				printf("facultad:%s\n",todo[i].facultad);
 				printf("carrera:%s\n",todo[i].carrer);
 				printf("codigo de la carrera:%s\n",todo[i].codigo);
 				printf("PONDERACIONES (%) PROCESO DE ADMISION 2018\n");
 				printf("la ponderacion del nem es de:%s\n",todo[i].nem);
 				printf("la ponderacion del ranking es de:%s\n",todo[i].rank);
 				printf("la ponderacion de lenguaje es de:%s\n",todo[i].leng);
 				printf("la ponderacion de matematica es de:%s\n",todo[i].mat);
 				printf("la ponderacion de historia es de:%s\n",todo[i].hist);
 				printf("la ponderacion de ciencias es de:%s\n",todo[i].cs);
 				printf("PTJE. MINIMO POSTULACION\n");
 				printf("el puntaje minimo ponderado es de:%s\n",todo[i].pond);
 				printf("el puntaje minimo de psu es de:%s\n",todo[i].psu);
 				printf("PTJE. PONDERADO PRIMER Y ULTIMO MATRIC. 2017");
 				printf("el puntaje maximo ponderado en 2017 fue de:%s\n",todo[i].max);
 				printf("el puntaje minimo ponderado en 2017 fue de:%s\n",todo[i].min);
 				printf("CUPOS\n");
 				printf("los cupos por puntaje psu son:%s\n",todo[i].cpsu);
 				printf("los cupos por bea (beca exelencia academica) son:%s\n",todo[i].bea);
 			}}
 		}
 		if (control==0){//si control es igual a 0 
 			printf("cargue las carreras primero (opcion2)");//se muestra mensaje para cargar las carreras
 		}
 		else{//si la condicional no funciona entonces muestra mensaje para reescribir la peticion del usuario
 			printf("revise que esta correctamente escrito lo que desea buscar (exactamente como muestra la lista de carreras respetando mayusculas)");
 		}
	}
}
	}
